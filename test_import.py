#!/usr/bin/env python

"""
A program to:
   * create an fdump.py file from fdump (just copy)
   * run a test program that imports fdump.py and dumps two lines to stdout
   * delete the fdump.py code
"""

import os
import shutil


# create "fdump.py" from "fdump"
shutil.copyfile("fdump", "fdump.py")

# import the file, print some data
import fdump
bseq = b'Now\tis\nthe time\nfor all good men\nto come to the aid of the party.'[:16]
print(fdump.hexdump_line(bseq, 0, 2))

# delete "fdump.py"
os.remove("fdump.py")
